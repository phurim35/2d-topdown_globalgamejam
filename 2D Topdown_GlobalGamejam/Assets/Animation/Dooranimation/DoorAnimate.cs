﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorAnimate : MonoBehaviour
{
    public bool isRunning = false;
    public Animator anima;
    public bool isENDDoor = false;
    public ItemCheck itemCheck;
    // Update is called once per frame
    void Update()
    {
        
        anima.SetBool("isOpen", isRunning);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!isENDDoor)
            if (collision.gameObject.CompareTag("Player"))
        {
            isRunning = false;
        }
        


        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isENDDoor)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                isRunning = true;
            }
        }
            
        else
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                itemCheck = collision.gameObject.GetComponent<ItemCheck>();
                if (itemCheck._itemInventory == itemCheck._itemScript[8])
                {
                    isRunning = true;
                    itemCheck._itemInventory = itemCheck._itemScript[0];
                    itemCheck.playerItemBox.GetComponent<Image>().sprite = itemCheck._itemInventory.ItemSprite;
                }
            }
        }

    }
}

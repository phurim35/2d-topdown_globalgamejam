﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LastRoom : MonoBehaviour
{
    public GameObject key;
    public GameObject eye;
    public GameObject boots;
    public GameObject coin;
    public ItemCheck itemCheck;
    public int currentCount=0;
    

    private void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            itemCheck = other.gameObject.GetComponent<ItemCheck>();

            if (itemCheck._itemInventory == itemCheck._itemScript[7])
            {
                itemCheck._itemInventory = itemCheck._itemScript[0];
                eye.SetActive(true);
                currentCount += 1;
                itemCheck.playerItemBox.GetComponent<Image>().sprite = itemCheck._itemInventory.ItemSprite;
            }
            if (itemCheck._itemInventory == itemCheck._itemScript[1])
            {
                itemCheck._itemInventory = itemCheck._itemScript[0];
                coin.SetActive(true);
                currentCount += 1;
                itemCheck.playerItemBox.GetComponent<Image>().sprite = itemCheck._itemInventory.ItemSprite;
            }
            if (itemCheck._itemInventory == itemCheck._itemScript[2])
            {
                itemCheck._itemInventory = itemCheck._itemScript[0];
                boots.SetActive(true);
                currentCount += 1;
                itemCheck.playerItemBox.GetComponent<Image>().sprite = itemCheck._itemInventory.ItemSprite;
            }
            if (currentCount == 3)
            {
                key.SetActive(true);
            }

        }
    }
    private void OnCollisionEnter2D(Collider2D other)
    {
        
    }
}

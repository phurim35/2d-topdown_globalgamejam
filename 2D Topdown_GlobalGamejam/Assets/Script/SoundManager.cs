﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{ public static SoundManager Instance { get; private set; }
  
    
    
    public const float MUTE_VOLUME = -80;
    [SerializeField]
    protected AudioMixer mixer;
    public AudioMixer Mixer { get { return mixer; } set { mixer = value; } }
    public AudioSource BGMSource { get; set; }
    #region
    public float MusicVolumeDefault { get; set; }

    protected float musicVolume;
    public float MusicVolume
    {
        get { return this.musicVolume; }
        set
        {
            this.musicVolume = value;
            SoundManager.Instance.Mixer.SetFloat("BGM", this.musicVolume);
        }
    }


    #endregion
    #region SFX Volume
    protected float masterSFXVolume;
    public float MasterSFXVolume
    {
        get { return this.masterSFXVolume; }
        set
        {
            this.masterSFXVolume = value;
            SoundManager.Instance.Mixer.SetFloat("SFX", this.masterSFXVolume);
        }
    }
    public float MasterSFXVolumeDefault { get; set; }
    #endregion
    private void Awake()
    {
     
            if (Instance == null)
            {
            Instance = this;
                DontDestroyOnLoad(gameObject);

            }
            else
            {
                Destroy(gameObject);
            }
       
        this.BGMSource = this.GetComponent<AudioSource>();

        float musicVolume;
        if (mixer.GetFloat("BGM", out musicVolume))
            SoundManager.Instance.MusicVolumeDefault = musicVolume;
        float masterSFXVolume;
        if (mixer.GetFloat("SFX", out masterSFXVolume))
            SoundManager.Instance.MasterSFXVolumeDefault = masterSFXVolume;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingTile : MonoBehaviour
{
    public bool isMoving ;
    public Animator animate;

    public bool isStaying;

    // Update is called once per frame
    private void Start()
    {
        isMoving = true;
    }
    void Update()
    {
        
        animate.SetBool("IsMoving", isMoving);
    }
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            isStaying = true;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            isStaying = false;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollider : MonoBehaviour
{
    public bool inDeadZone;
    
    public PushingAnimate pushingAnimate;
    public SpawnPlayer spawnPlayer;
    Collider2D player;
    bool dead;

    private void Start()
    {
        
    }

    private void Update()
    {
        if (dead == true)
            this.transform.position = spawnPlayer._spawnPoint[spawnPlayer.random].transform.position;
        dead = false;

    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Check"))
        {
            inDeadZone = true;
        }
    }
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("MovingTile"))
        {
  
            inDeadZone = false;
            //Debug.Log("Alive");
            
        }

        if (inDeadZone == true)
        {
            if (inDeadZone == true && other.gameObject.CompareTag("Dead"))
            {

                //Dead here
                Debug.Log("Dead");
                dead = true;

            }
        }
  
            if (other.gameObject.CompareTag("Spike"))
            {
            bool istakeDMG = other.GetComponent<DMGformTrap>().isTakeDMG;
            if(istakeDMG)
            {
                dead = true;
                //Dead here
                Debug.Log("Dead");
            }
           
            }
        



    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("MovingTile"))
        {
            inDeadZone = true;
            


        }
        dead = false;
        //if (other.gameObject.CompareTag("Trade"))
        //{
        //    pushingAnimate.canGetItem = false;
        //}
    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushingAnimate : MonoBehaviour
{
    public bool isPushing;
    public bool canGetItem;
    public Collider2D trade;
    public Animator animate;
    public int currentPlayer=0;

    public GameObject arrow;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        animate.SetBool("WasPush", isPushing);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        
        if (other.gameObject.CompareTag("Player"))
        {
            currentPlayer = currentPlayer + 1;
            if (currentPlayer == 2)
            {
                isPushing = true;
                canGetItem = true;
                Debug.Log("You Can get Item");
                arrow.SetActive(true);
            }
        }
        
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            isPushing = false;
            currentPlayer = currentPlayer - 1;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemCheck : MonoBehaviour
{
    public ItemObject _itemInventory;
    public ItemObject[] _itemScript;
    public GameObject pickupText;
    public GameObject dropText;
    public GameObject shopText;
    public GameObject playerItemBox;
    public GameObject TradeBox;
    public GameObject[] TradeItem;
    public GameObject[] GoalItem;
    private Collider2D mycolli;

    void Update()
    {
        DropItem();
        ItemFuntion();
    }

    private void OnTriggerEnter2D(Collider2D other) {
       
        if (other.gameObject.tag == "Item"){
            pickupText.SetActive(true);
            mycolli = other;
        }
        if(other.gameObject.tag == "Shop"){
            shopText.SetActive(true);
            TradeBox.SetActive(true);
            mycolli = other;
        }

        //Shop
        if(other.gameObject.name == "ShopGold"){
            TradeItem[0].SetActive(true);
        }
        if(other.gameObject.name == "ShopBoots"){
            TradeItem[3].SetActive(true);
        }
        if(other.gameObject.name == "ShopBottle"){
            TradeItem[1].SetActive(true);
        }
        if(other.gameObject.name == "ShopEye"){
             TradeItem[4].SetActive(true);
        }
        if(other.gameObject.name == "ShopCoin"){
             TradeItem[5].SetActive(true);
        }

    }
    private void OnTriggerExit2D(Collider2D other) {
        mycolli = null;
        if (other.gameObject.tag == "Item"){
            pickupText.SetActive(false);
         
        }
        if(other.gameObject.tag == "Shop"){
            shopText.SetActive(false);
            TradeBox.SetActive(false);
        }

        //Shop
        if(other.gameObject.name == "ShopGold"){
             TradeItem[0].SetActive(false);
        }
        if(other.gameObject.name == "ShopBoots"){
             TradeItem[3].SetActive(false);
        }
        if(other.gameObject.name == "ShopBottle"){
             TradeItem[1].SetActive(false);
        }
        if(other.gameObject.name == "ShopEye"){
             TradeItem[4].SetActive(false);
        }
        if(other.gameObject.name == "ShopCoin"){
             TradeItem[5].SetActive(false);
        }
    }


    public void ItemFuntion()
    {
        if (mycolli != null)
        {
            if (mycolli.gameObject.tag == "Item")
            {
                if (this.gameObject.name == "Player1")
                {
                    if (Input.GetKeyDown(KeyCode.E))
                    {

                        Debug.Log("Player 1 Got item");
                        if (_itemInventory != _itemScript[0])
                        {
                            GameObject obj1 = Instantiate(_itemInventory.ItemGameObject, this.gameObject.transform.position, Quaternion.identity);
                        }

                        //CheckGetItemP1

                        if (mycolli.gameObject.name == "Coin" || mycolli.gameObject.name == "Coin(Clone)")
                        {
                            _itemInventory = _itemScript[1];
                        }
                        else if (mycolli.gameObject.name == "Boots" || mycolli.gameObject.name == "Boots(Clone)")
                        {
                            _itemInventory = _itemScript[2];
                        }
                        else if (mycolli.gameObject.name == "Knife" || mycolli.gameObject.name == "Knife(Clone)")
                        {
                            _itemInventory = _itemScript[3];
                        }
                        else if (mycolli.gameObject.name == "Gold" || mycolli.gameObject.name == "Gold(Clone)")
                        {
                            _itemInventory = _itemScript[4];
                        }
                        else if (mycolli.gameObject.name == "Ring" || mycolli.gameObject.name == "Ring(Clone)")
                        {
                            _itemInventory = _itemScript[5];
                        }
                        else if (mycolli.gameObject.name == "Bottle" || mycolli.gameObject.name == "Bottle(Clone)")
                        {
                            _itemInventory = _itemScript[6];
                        }
                        else if (mycolli.gameObject.name == "Eye" || mycolli.gameObject.name == "Eye(Clone)")
                        {
                            _itemInventory = _itemScript[7];
                        }
                        else if (mycolli.gameObject.name == "Key" || mycolli.gameObject.name == "Key(Clone)")
                        {
                            _itemInventory = _itemScript[8];
                        }

                        playerItemBox.GetComponent<Image>().sprite = _itemInventory.ItemSprite;
                        dropText.SetActive(true);

                        Destroy(mycolli.gameObject);

                        return;
                    }
                }

                if (this.gameObject.name == "Player2")
                {
                    if (Input.GetKeyDown(KeyCode.Keypad0))
                    {
                        Debug.Log("Player 2 Got item");
                        if (_itemInventory != _itemScript[0])
                        {
                            GameObject obj1 = Instantiate(_itemInventory.ItemGameObject, this.gameObject.transform.position, Quaternion.identity);
                        }

                        //CheckGetItemP2

                        if (mycolli.gameObject.name == "Coin" || mycolli.gameObject.name == "Coin(Clone)")
                        {
                            _itemInventory = _itemScript[1];
                        }
                        else if (mycolli.gameObject.name == "Boots" || mycolli.gameObject.name == "Boots(Clone)")
                        {
                            _itemInventory = _itemScript[2];
                        }
                        else if (mycolli.gameObject.name == "Knife" || mycolli.gameObject.name == "Knife(Clone)")
                        {
                            _itemInventory = _itemScript[3];
                        }
                        else if (mycolli.gameObject.name == "Gold" || mycolli.gameObject.name == "Gold(Clone)")
                        {
                            _itemInventory = _itemScript[4];
                        }
                        else if (mycolli.gameObject.name == "Ring" || mycolli.gameObject.name == "Ring(Clone)")
                        {
                            _itemInventory = _itemScript[5];
                        }
                        else if (mycolli.gameObject.name == "Bottle" || mycolli.gameObject.name == "Bottle(Clone)")
                        {
                            _itemInventory = _itemScript[6];
                        }
                        else if (mycolli.gameObject.name == "Eye" || mycolli.gameObject.name == "Eye(Clone)")
                        {
                            _itemInventory = _itemScript[7];
                        }
                        else if (mycolli.gameObject.name == "Key" || mycolli.gameObject.name == "Key(Clone)")
                        {
                            _itemInventory = _itemScript[8];
                        }


                        playerItemBox.GetComponent<Image>().sprite = _itemInventory.ItemSprite;
                        dropText.SetActive(true);
                        Destroy(mycolli.gameObject);
                        mycolli = null;
                    }
                }
            }

            if (mycolli.gameObject.tag == "Shop")
            {
                if (this.gameObject.name == "Player1")
                {

                    if (Input.GetKeyDown(KeyCode.R))
                    {
                        //ExchangeShopP1
                        if (mycolli.gameObject.name == "ShopGold")
                        {

                            if (_itemInventory == _itemScript[1])
                            {
                                _itemInventory = _itemScript[4];
                                Destroy(mycolli.gameObject);
                                playerItemBox.GetComponent<Image>().sprite = _itemInventory.ItemSprite;
                                return;
                            }
                        }

                        if (mycolli.gameObject.name == "ShopBoots")
                        {

                            if (_itemInventory == _itemScript[4])
                            {
                                _itemInventory = _itemScript[2];
                                Destroy(mycolli.gameObject);
                                playerItemBox.GetComponent<Image>().sprite = _itemInventory.ItemSprite;
                                return;
                            }
                        }

                        if (mycolli.gameObject.name == "ShopBottle")
                        {

                            if (_itemInventory == _itemScript[2])
                            {
                                _itemInventory = _itemScript[6];
                                Destroy(mycolli.gameObject);
                                playerItemBox.GetComponent<Image>().sprite = _itemInventory.ItemSprite;
                                return;
                            }
                        }

                        if (mycolli.gameObject.name == "ShopEye")
                        {

                            if (_itemInventory == _itemScript[4])
                            {
                                _itemInventory = _itemScript[7];
                                Destroy(mycolli.gameObject);
                                playerItemBox.GetComponent<Image>().sprite = _itemInventory.ItemSprite;
                                return;
                            }
                        }
                        if (mycolli.gameObject.name == "ShopCoin")
                        {

                            if (_itemInventory == _itemScript[6])
                            {
                                _itemInventory = _itemScript[1];
                                Destroy(mycolli.gameObject);
                                playerItemBox.GetComponent<Image>().sprite = _itemInventory.ItemSprite;
                                return;
                            }
                        }

                    }
                }

                if (this.gameObject.name == "Player2")
                {
                    if (Input.GetKeyDown(KeyCode.Keypad3))
                    {
                        //ExchangeShopP2
                        if (mycolli.gameObject.name == "ShopGold")
                        {

                            if (_itemInventory == _itemScript[1])
                            {
                                _itemInventory = _itemScript[4];
                                Destroy(mycolli.gameObject);
                            }
                        }

                        if (mycolli.gameObject.name == "ShopBoots")
                        {

                            if (_itemInventory == _itemScript[4])
                            {
                                _itemInventory = _itemScript[2];
                                Destroy(mycolli.gameObject);
                            }
                        }
                        playerItemBox.GetComponent<Image>().sprite = _itemInventory.ItemSprite;
                    }
                }
            }
        }
    }
    public void DropItem()
    {
        //DropItem
        if (this.gameObject.name == "Player1")
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                GameObject obj1 = Instantiate(_itemInventory.ItemGameObject, this.gameObject.transform.position, Quaternion.identity);
                _itemInventory = _itemScript[0];
                playerItemBox.GetComponent<Image>().sprite = _itemInventory.ItemSprite;
                dropText.SetActive(false);
            }
        }
        if (this.gameObject.name == "Player2")
        {
            if (Input.GetKeyDown(KeyCode.Keypad2))
            {
                GameObject obj1 = Instantiate(_itemInventory.ItemGameObject, this.gameObject.transform.position, Quaternion.identity);
                _itemInventory = _itemScript[0];
                playerItemBox.GetComponent<Image>().sprite = _itemInventory.ItemSprite;
                dropText.SetActive(false);
            }
        }
    }
}
﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ItemObject", 
menuName = "ScriptableObjects / ItemObject", order = 1)]
public class ItemObject : ScriptableObject
{
    public enum Type {None,Coin,Boots,Knife,Gold,Ring,Bottle,Eye,Key};
    public Type Typeof;
    public GameObject ItemGameObject;
    public Sprite ItemSprite;

}
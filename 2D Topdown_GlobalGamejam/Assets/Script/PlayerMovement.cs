﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ControllPlayer
{
    Player1, Player2
}
public class PlayerMovement : MonoBehaviour
{
    public float movespeed=5f;
    private Rigidbody2D rb;
    private Animator animate;
    private Vector2 movement;
    
    public ControllPlayer Player;


    private void Start()
    {
        rb =this.gameObject.GetComponent<Rigidbody2D>();
        animate = this.gameObject.GetComponent<Animator>();


    }
    void Update()
    {
        movement = Vector2.zero;
        if (Player == ControllPlayer.Player1)
        {
            movement.x = Input.GetAxisRaw("Horizontal1");
            movement.y = Input.GetAxisRaw("Vertical1");
        }
        if (Player == ControllPlayer.Player2)
        {
            movement.x = Input.GetAxisRaw("Horizontal2");
            movement.y = Input.GetAxisRaw("Vertical2");
        }

    }
    void FixedUpdate()
    {
        rb.MovePosition(rb.position + movement * movespeed * Time.deltaTime);
        UpdateAnimation();
       if(movement.x!=0)
        UpdateFlip(movement);
    }

   private void UpdateAnimation()
   {
       if (movement != Vector2.zero)
       {

            animate.SetBool("IsWalk", true);
       }
       else
            animate.SetBool("IsWalk", false);
   }
    private void UpdateFlip(Vector2 direction)
    {
        SpriteRenderer sprt = this.GetComponent<SpriteRenderer>();
        
        if (direction.x < 0)
        {

            sprt.flipX = true;
        }
        else
            sprt.flipX = false;
            

    }

}

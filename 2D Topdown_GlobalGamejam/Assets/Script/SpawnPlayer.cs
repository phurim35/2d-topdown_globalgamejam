﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPlayer : MonoBehaviour
{
    public GameObject _Player1;
    public GameObject _Player2;
    public GameObject[] _spawnPoint;
    private int ck;
    public int random;

    void Start()
    {
        random = Random.Range(0,_spawnPoint.Length);
        ck = random;

        _Player1.transform.position= _spawnPoint[random].transform.position;

        while(random == ck) {random = Random.Range(0,_spawnPoint.Length);}

        _Player2.transform.position = _spawnPoint[random].transform.position;

    }
    private void Update()
    {
        random = Random.Range(0, 3);
        ck = random;
    }
}

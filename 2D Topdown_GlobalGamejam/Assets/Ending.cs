﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ending : MonoBehaviour
{
    public int currentPlayer=0;
    public GameObject ui;
    // Start is called before the first frame update
    void Start()
    {
        currentPlayer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (currentPlayer == 2)
        {
            ui.SetActive(true);
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            currentPlayer += 1;
        }
    }
}
